"""Shows advertising by watching the log file created by `demo.py`."""

import time
from pathlib import Path
import subprocess
from collections import Counter
import cv2
import random

UNKNOWN = 'unknown'
IDLE = 'idle'


class DetectionVoting:
    def __init__(self, num_dets=5, time_btw_votings=0.1, det_lifetime=5.0):
        """
        Keyword Arguments:
            num_dets {int} -- The number of detections used in the majority voting
            det_lifetime {float} -- The number of seconds that a detection is kept
            time_btw_votings {float} -- The minimum number of seconds before a new voting can be organised
        """
        self.num_dets = num_dets
        self.det_lifetime = det_lifetime
        self.dets = []

        self.time_btw_votings = time_btw_votings
        self.last_time_voted = -1
        self.result = IDLE

    def on_detection(self, gender):
        now = time.time()
        self.dets.append({'gen': gender, 'time': now})
        self.refresh()

    def refresh(self):
        now = time.time()

        # Remove detections that are too old
        self.dets = [d for d in self.dets
                     if now - d['time'] <= self.det_lifetime]

        if len(self.dets) < self.num_dets:
            # There are not enough detections
            # We're in idle state
            self.result = IDLE
            return

        if now - self.last_time_voted < self.time_btw_votings:
            # There was a voting too recently
            # Maintain the current state
            return

        # Organize voting
        votes = Counter([d['gen']
                         for d in self.dets[:self.num_dets]])
        self.result = votes.most_common(1)[0][0]

        # Remove the oldest detection
        self.dets = self.dets[1:] if len(self.dets) > 1 else []


class AdShower:
    def __init__(self, imgmap: dict, mix_keys=[]):
        """        
        Arguments:
            imgmap {dict} -- mapping between a detection result and a corresponding image.
            Should at least contain the keys defined by `IDLE` and `UNKNOWN`
            mix_keys {list} -- list of keys between which shown ads will be chosen randomly
        """
        if IDLE not in imgmap:
            raise ValueError(f'{IDLE} should be a key of `imgmap`')
        if UNKNOWN not in imgmap:
            raise ValueError(f'{UNKNOWN} should be a key of `imgmap`')

        self.imgmap = imgmap
        self.mix_keys = mix_keys
        self.now_showing = None
        self.window_name = 'Ad'
        cv2.namedWindow(self.window_name, cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty(
            self.window_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        self.display(IDLE)

    def display(self, result):
        prev_showing = self.now_showing

        if result in self.mix_keys:
            self.now_showing = random.choice(self.mix_keys)
        else:
            self.now_showing = result

        if self.now_showing not in self.imgmap:
            self.now_showing = UNKNOWN

        if self.now_showing == prev_showing:
            return

        img = cv2.imread(self.imgmap[self.now_showing])
        cv2.imshow(self.window_name, img)
        cv2.waitKey(1)
