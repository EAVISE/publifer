import inprocout as ipo

#
# Where do we take the images from?
#
inputter = ipo.WebcamInputter(camera=0)
# Use the DummyInputter for testing on your own images
# inputter = DummyInputter(
#     img_dir='/home/fdf/projects/publifer/demo/_data/test_women')

#
# Configuration of the outputs (images, logs,...)
#
outputter = ipo.Outputter(imgmap={
    ipo.MALE: '/home/fdf/projects/publifer/demo/_data/male.jpg',
    ipo.FEMALE: '/home/fdf/projects/publifer/demo/_data/female.jpg',
    ipo.IDLE: '/home/fdf/projects/publifer/demo/_data/ucll_logo.png',
    ipo.UNKNOWN: '/home/fdf/projects/publifer/demo/_data/ucll_logo.png'
},
    debug=True,
    num_dets=4,
    mix_keys=[ipo.MALE, ipo.FEMALE],
    out_csv='/home/fdf/projects/publifer/demo/logs/log.csv')

# Define the processor and run
# This shouldn't be changed
proc = ipo.Processor(inputter=inputter, outputter=outputter)
proc.run()