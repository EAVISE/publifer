from flutil.extract.torch import TorchModuleExtractor
from flutil.detect.object.lightnet import YoloV3
import torch
import cv2
from PIL import Image
from pathlib import Path
import pandas as pd
import numpy as np
import datetime as dt
from adshow import AdShower, DetectionVoting, UNKNOWN, IDLE

MALE = 'male'
FEMALE = 'female'


class Processor:
    def __init__(self, inputter, outputter, det_bs=1, clf_bs=1,
                 ar_thresh=3, use_thresh=False, thresh=0.5):
        """Wrapper around a demo set-up for gender classification.

        Keyword Arguments:
            inputter -- A generator providing images
            ar_thresh {float} -- The minimal ratio height / width for a person detection to pass. This is to apply the classification only on full bodies, not on faces (default: {3})
        """
        self.ar_thresh = ar_thresh
        self.use_thresh = use_thresh
        self.thresh = thresh
        self.inputter = inputter
        self.outputter = outputter

        # Configure detector
        COCO_LABELS = '/home/fdf/projects/publifer/demo/_etc/labels.txt'
        label_map = {i: l.strip()
                     for i, l in enumerate(Path(COCO_LABELS).open())}
        self.detector = YoloV3(imgs=[], weights='/home/fdf/projects/publifer/demo/_etc/yolov3.weights',
                               bs=det_bs,
                               conf_thresh=0.5, num_classes=80,
                               class_label_map=label_map)

        # Configure attribute extractor
        model = torch.load(
            '/home/fdf/projects/publifer/demo/_etc/stage-2_rn50_30epoch_128x64.pth')
        self.extractor = TorchModuleExtractor(
            imgs=[], model=model, bs=clf_bs,
            transform=lambda im: im.resize((64, 128)))

    def run(self):
        for img in iter(self.inputter):
            # Detect
            df_dets = self.detect([img])

            # We only need person detections
            df_dets = df_dets[df_dets['detection'].apply(
                lambda det: det.label == 'person' if det is not None else False)]

            # Only use detections with an elongated aspect ratio
            # to only use "full bodies"
            df_dets = df_dets[
                df_dets['detection'].apply(
                    lambda det: det.height / det.width > self.ar_thresh)
            ]

            if len(df_dets) == 0:
                continue

            # When multiple persons are detected in an image, use the largest detection
            df_dets['det_size'] = df_dets['detection'].apply(
                lambda det: det.area)
            df_dets = (df_dets
                       .sort_values(by='det_size', ascending=False)
                       .groupby('image')
                       .first()
                       .reset_index())

            if len(df_dets) == 0:
                continue

            # Extract
            df_descrs = self.extract([img],
                                     df_dets.groupby('image').agg(list)['detection'].tolist())

            # Classify
            df_clf = self.classify(
                df_descrs['descriptor'], use_thresh=self.use_thresh, thresh=self.thresh)

            # Output
            df = df_descrs.merge(df_clf,
                                 left_index=True,
                                 right_index=True)

            df['nd_img'] = [np.array(
                self.extractor.transform(
                    Image.fromarray(
                        self.extractor.prep_nd_img(img,
                                                   row['detection']))))
                            for _, row in df.iterrows()]

            for _, row in df.iterrows():
                self.outputter(
                    row[['pred', 'male_score', 'female_score', 'nd_img']])

    def detect(self, imgs):
        self.detector.imgs = imgs
        dets = self.detector.get_detections()
        return dets

    def extract(self, imgs, dets):
        self.extractor.imgs = imgs
        self.extractor.dets = dets
        descrs = self.extractor.get_descriptors()
        return descrs

    def classify(self, descrs, use_thresh=False, thresh=0.5):
        FEMALE_IDX = 0
        MALE_IDX = 1

        male_scores = [descr[MALE_IDX] for descr in descrs]
        female_scores = [descr[FEMALE_IDX] for descr in descrs]

        df_clf = pd.DataFrame({
            'male_score': male_scores,
            'female_score': female_scores,
        })

        df_clf['pred'] = df_clf.apply(lambda row: self._get_gender(
            row['male_score'],
            row['female_score'],
            use_thresh=use_thresh,
            thresh=thresh), axis=1)

        return df_clf

    def _get_gender(self, male_score, female_score, use_thresh=False, thresh=0.5):
        if male_score > thresh and female_score < thresh:
            return 'male'
        elif male_score < thresh and female_score > thresh:
            return 'female'
        elif not use_thresh:
            return 'male' if male_score > female_score else 'female'
        else:
            return 'unknown'


class Outputter:
    def __init__(self, imgmap, debug=False, out_csv='logs/log.csv', out_img_dir='logs/imgs/',
                 mix_keys=[], num_dets=5, time_btw_votings=0.1, det_lifetime=5.0):
        self.out_csv = Path(out_csv)
        self.out_img_dir = Path(out_img_dir)
        self.debug = debug
        self.det_voting = DetectionVoting(num_dets=num_dets,
                                          time_btw_votings=time_btw_votings,
                                          det_lifetime=det_lifetime)
        self.ad_shower = AdShower(imgmap=imgmap, mix_keys=mix_keys)

    def writer(self, row):
        """
        Log the classification results contained in the Series objects.
        """
        # Save the images
        now = dt.datetime.now()
        row['time'] = str(now)
        # if not self.out_img_dir.exists():
        #     self.out_img_dir.mkdir(parents=True)

        # row['image'] = (self.out_img_dir / (str(now) + '.jpg')).resolve()
        # im = Image.fromarray(row['nd_img'])
        # im.save(row['image'])

        # Log the classifications
        if not self.out_csv.parent.exists():
            self.out_csv.parent.mkdir(parents=True)

        mode = 'a' if self.out_csv.exists() else 'w'
        with self.out_csv.open(mode) as f:
            (row.drop(labels=['nd_img'])
                .to_frame()
                .transpose()
                .to_csv(f, header=(mode == 'w'), index=False))

    def refresh_ad(self):
        self.det_voting.refresh()
        self.ad_shower.display(self.det_voting.result)

    def __call__(self, row: pd.Series):
        """Log the classification results contained in a DataFrame row.

        Arguments:
            row {pd.Series} -- A `Series` containing at least these keys:
                - `nd_img`: a numpy array with the cropped detection
                - `pred`: the prediction
                - `male_score`: the score that the detection is male
                - `female_score`: the score that the detection is female
        """
        # Refresh the shown ad
        prev_result = self.det_voting.result
        self.det_voting.on_detection(row['pred'])

        if prev_result != self.det_voting.result:
            self.ad_shower.display(self.det_voting.result)

        if self.debug:
            font = cv2.FONT_HERSHEY_SIMPLEX
            bottomLeftCornerOfText = (10, 100)
            fontScale = 1
            fontColor = (255, 255, 255)
            lineType = 2

            nd_img = row['nd_img']
            cv2.putText(nd_img, row['pred'][0],
                        bottomLeftCornerOfText,
                        font,
                        fontScale,
                        fontColor,
                        lineType)
            cv2.imshow('frame', nd_img)

        row['shown'] = self.ad_shower.now_showing

        self.writer(row)


class WebcamInputter:
    def __init__(self, camera=0, frame_delay=100):
        self.frame_delay = frame_delay

        # Set up video capture
        self.capture = cv2.VideoCapture(camera)
        if not self.capture.isOpened():
            self.capture.open()

    def __iter__(self):
        """Yield an image as taken from a capturing device.

        :yields: a PIL image
        """
        while(True):
            # Read batch_size frames
            _, frame = self.capture.read()
            if cv2.waitKey(self.frame_delay) & 0xFF == ord('q'):
                self.capture.release()
                cv2.destroyAllWindows()
                break
            im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            yield im


class DummyInputter:
    def __init__(self, img_dir):
        self.img_gen = (p for p in Path(img_dir).glob('*')
                        if p.suffix.lower() in ['.jpg', '.jpeg', '.bmp', '.png'])

    def __iter__(self):
        """Yield an image.

        :yields: a PIL image
        """
        while(True):
            img = next(self.img_gen)
            im = Image.open(img)
            cv2.waitKey(500)
            yield np.array(im)
