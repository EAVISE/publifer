import torch.utils.data as data
from PIL import Image
from typing import List
import numpy as np
import pandas as pd


class MarketAttr(data.Dataset):
    def __init__(self, imgs: List[str],
                 targets: List[np.array],
                 transform=None):
        """PyTorch Dataset for the MarketAttribute dataset.

        :param list imgs: list of paths of the image files
        :param list targest: list of targets (i.e. the attributes), one
        `np.array` per image
        :param tansform: (optional) a callable with which each image needs to
        be transformed
        """
        if len(imgs) != len(targets):
            raise ValueError('imgs and attributes should '
                             'have the same length.')

        self.imgs = imgs
        self.targets = targets
        self.transform = transform

    def __getitem__(self, index):
        img = Image.open(self.imgs[index])
        target = self.targets[index]

        if self.transform is not None:
            img = self.transform(img)

        return img, target

    def __len__(self):
        return len(self.imgs)


def get_images():
    dfs = {x: pd.read_pickle(f'cache/Market_attr_{x}.pkl')
           for x in ['train', 'test']}
    images = {x: df['image'].apply(lambda p: str(p)).values
              for x, df in dfs.items()}
    return images


def get_targets():
    dfs = {x: pd.read_pickle(f'cache/Market_attr_{x}.pkl')
           for x in ['train', 'test']}
    targets = {x: (df
                   .drop(columns=['image', 'label', 'attrs'])
                   .values == 'yes').astype('float32')
               for x, df in dfs.items()}
    return targets
